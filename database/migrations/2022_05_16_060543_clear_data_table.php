<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClearDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $payments = \App\Models\Payment::all();
        foreach ($payments as $payment) {
            \App\Models\Payment::destroy($payment->id);
        }
        $orders = \App\Models\Order::all();
        foreach ($orders as $order) {
            \App\Models\Order::destroy($order->id);
        }
        $cards = \App\Models\Card::all();
        foreach ($cards as $card) {
            \App\Models\Card::destroy($card->id);
        }
        $reviews = \App\Models\Review::all();
        foreach ($reviews as $review){
            \App\Models\Review::destroy($review->id);
        }
        $files = \App\Models\File::all();
        foreach ($files as $file){
            \App\Models\File::destroy($file->id);
        }
        $products = \App\Models\Product::all();
        foreach ($products as $product){
            \App\Models\Product::destroy($product->id);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
