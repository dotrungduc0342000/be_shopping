<?php
namespace Database\Seeders;

use App\Models\Product;
use DB;

class ProductSeeder extends DatabaseSeeder {

    public function run()
    {
        for($i = 1; $i <= 15; $i++){
            $data = [
                'category_id' => random_int(1, 3),
                'user_create' => 1,
                'name' => 'Product ' . $i,
                'description' => 'Rất tốt',
                'quantity' => random_int(10, 30),
                'price' => '1500000',
                'discount_price' => random_int(100000, 500000),
            ];
            Product::insert($data);
        }
    }

}
