<?php
namespace Database\Seeders;

use App\Models\Order;
use App\Models\Product;
use App\Models\review;
use DB;

class OrderSeeder extends DatabaseSeeder {

    public function run()
    {
            $data = [
                [   'product_id' => 1,
                    'user_id' => 2,
                    'amount' => 6,
                    'total_price' => (Product::find(1)->price - Product::find(1)->discount_price) * 6,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ],
                [   'product_id' => 2,
                    'user_id' => 2,
                    'amount' => 6,
                    'total_price' => (Product::find(2)->price - Product::find(2)->discount_price) * 6,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ],
                [   'product_id' => 3,
                    'user_id' => 2,
                    'amount' => 6,
                    'total_price' => (Product::find(3)->price - Product::find(3)->discount_price) * 6,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ],
                [   'product_id' => 4,
                    'user_id' => 3,
                    'amount' => 8,
                    'total_price' => (Product::find(4)->price - Product::find(4)->discount_price) * 8,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ],
                [   'product_id' => 5,
                    'user_id' => 3,
                    'amount' => 8,
                    'total_price' => (Product::find(5)->price - Product::find(5)->discount_price) * 8,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ],
                [   'product_id' => 6,
                    'user_id' => 3,
                    'amount' => 8,
                    'total_price' => (Product::find(6)->price - Product::find(6)->discount_price) * 8,
                    'status' => 'pending',
                    'shipped_date' => '2022-04-26',
                ]
            ];
            Order::insert($data);
    }

}
