<?php
namespace Database\Seeders;

use App\Models\Payment;
use DB;

class PaymentSeeder extends DatabaseSeeder {

    public function run()
    {
        $data = [
            [   'order_id' => 1,
                'type' => 'off',
                'status' => 'unpaid',
            ],
            [   'order_id' => 2,
                'type' => 'off',
                'status' => 'unpaid',
            ],
            [   'order_id' => 3,
                'type' => 'off',
                'status' => 'unpaid',
            ],
            [   'order_id' => 4,
                'type' => 'off',
                'status' => 'unpaid',
            ],
            [   'order_id' => 5,
                'type' => 'off',
                'status' => 'unpaid',
            ],
            [   'order_id' => 6,
                'type' => 'off',
                'status' => 'unpaid',
            ],
        ];
        Payment::insert($data);
    }

}
