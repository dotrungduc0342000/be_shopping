<?php
namespace Database\Seeders;

use App\Models\Product;

use App\Models\Review;
use DB;

class ReviewSeeder extends DatabaseSeeder {

    public function run()
    {
        for($i = 1; $i <= 15; $i++){
            $data = [
                [   'product_id' => $i,
                    'user_id' => 2,
                    'obj_model' => 'product',
                    'content' => 'Rất tốt',
                    'rate_number' => 5,
                    'status' => 'publish',
                ],
                [   'product_id' => $i,
                    'user_id' => 3,
                    'obj_model' => 'product',
                    'content' => 'Hài lòng',
                    'rate_number' => 4,
                    'status' => 'publish',
                ],
            ];
            Review::insert($data);
        }
    }

}
