<?php
namespace Database\Seeders;

use App\Models\Card;
use App\Models\Product;
use DB;

class CardSeeder extends DatabaseSeeder {

    public function run()
    {
        $data = [
            [   'product_id' => 8,
                'user_id' => 2,
                'amount' => 6,
                'total_price' => (Product::find(8)->price - Product::find(8)->discount_price) * 6,
            ],
            [   'product_id' => 10,
                'user_id' => 2,
                'amount' => 6,
                'total_price' => (Product::find(10)->price - Product::find(10)->discount_price) * 6,
            ],
            [   'product_id' => 12,
                'user_id' => 3,
                'amount' => 8,
                'total_price' => (Product::find(12)->price - Product::find(12)->discount_price) * 8,
            ],
            [   'product_id' => 9,
                'user_id' => 3,
                'amount' => 8,
                'total_price' => (Product::find(9)->price - Product::find(9)->discount_price) * 8,
            ],
        ];
        Card::insert($data);
    }

}
