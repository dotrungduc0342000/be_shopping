<?php
namespace Database\Seeders;

use App\Models\Category;
use DB;

class CategorySeeder extends DatabaseSeeder {

    public function run()
    {
        $categories = [
            [
                'user_create' => 1,
                'name' => 'Điện thoại',
                'description' => 'Rất tốt'
            ],
            [
                'user_create' => 1,
                'name' => 'Máy tính',
                'description' => 'Rất tốt'
            ],
            [
                'user_create' => 1,
                'name' => 'Tai nghe',
                'description' => 'Rất tốt'
            ],
        ];
        Category::insert($categories);
    }

}
