<?php
namespace Database\Seeders;

use App\Models\User;
use DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends DatabaseSeeder {

    public function run()
    {
        $users = [
            [
                'first_name' => '',
                'last_name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'type' => User::TYPE_ADMIN,
                'phone' => '123456789',
                'address' => 'default address',
                'status' => User::TYPE_STATUS_PUBLISH,
            ],
            [
                'first_name' => 'customer',
                'last_name' => '01',
                'email' => 'customer01@gmail.com',
                'password' => Hash::make('123456'),
                'type' => User::TYPE_CUSTOMER,
                'phone' => '123456789',
                'address' => 'default address',
                'status' => User::TYPE_STATUS_PUBLISH,
            ],
            [
                'first_name' => 'customer',
                'last_name' => '02',
                'email' => 'customer02@gmail.com',
                'password' => Hash::make('123456'),
                'type' => User::TYPE_CUSTOMER,
                'phone' => '123456789',
                'address' => 'default address',
                'status' => User::TYPE_STATUS_PUBLISH,
            ],
        ];
        User::insert($users);
    }

}
