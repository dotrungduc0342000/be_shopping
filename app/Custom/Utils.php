<?php
namespace App\Custom;

use File;

class Utils
{
    public static function uploadFile($file)
    {
        $file_path = 'upload/file';
        $file_name = $file->getClientOriginalName();
        $file_name = str_replace(' ', '', $file_name);
        if(File::exists($file_path . '/' . $file_name)){
            $arr = explode('.', $file_name);
            $file_name = $arr[0] . '_' . time() . '.' . array_pop($arr);
        }
        $file->move(public_path($file_path) , $file_name);
        return [
            'file_name' => $file_name,
            'file_path' => $file_path . '/' . $file_name
        ];
    }
    public static function destroyFile($file_path)
    {
        if( File::exists($file_path) ) {
            File::delete($file_path);
            return true;
        }
        return false;
    }
    public static function checkExtensionFile($file)
    {
        $fileExtension = [
            'jpg',
            'jpeg',
            'bmp',
            'png',
            'gif',
            'svg'
        ];
        if(in_array($file->getClientOriginalExtension(), $fileExtension)) return true;
        return false;
    }
    public static function createNewRecordFileProduct($file, $obj_id,  $type)
    {
        $fileUpload = self::uploadFile($file);
        $newRecordFile = new \App\Models\File([
            'obj_model' => \App\Models\File::OBJ_MODEL_PRODUCT,
            'user_id' => null,
            'product_id' => $obj_id,
            'name' => $fileUpload['file_name'],
            'mine' => $file->getClientMimeType(),
            'file_path' => $fileUpload['file_path'],
            'type' => $type,
            'status' => \App\Models\File::STATUS_PUBLISH,
        ]);
        $newRecordFile->save();
        return $newRecordFile;
    }
    public static function createNewRecordFileUser($file, $obj_id)
    {
        $fileUpload = self::uploadFile($file);
        $newRecordFile = new \App\Models\File([
            'obj_model' => \App\Models\File::OBJ_MODEL_USER,
            'user_id' => $obj_id,
            'product_id' => null,
            'name' => $fileUpload['file_name'],
            'mine' => $file->getClientMimeType(),
            'file_path' => $fileUpload['file_path'],
            'type' => \App\Models\File::TYPE_FILE_MAIN,
            'status' => \App\Models\File::STATUS_PUBLISH,
        ]);
        $newRecordFile->save();
        return $newRecordFile;
    }
    public static function updateRecordFile($file, $id)
    {
        $fileUpdate = \App\Models\File::find($id);
        self::destroyFile($fileUpdate->file_path);
        $fileUpload = self::uploadFile($file);
        $fileUpdate->name = $fileUpload['file_name'];
        $fileUpdate->mine = $file->getClientMimeType();
        $fileUpdate->file_path = $fileUpload['file_path'];
        $fileUpdate->update();
        return $fileUpdate;
    }
    public static function deleteRecordFile($id)
    {
        $file = \App\Models\File::find($id);
        self::destroyFile($file->file_path);
        $fileDes = \App\Models\File::destroy($file->id);
        if($fileDes) return true;
        return false;
    }
}
