<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payments = Payment::all();
        return \App\Http\Resources\Payment::collection($payments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $payment = new Payment([
                'order_id' => $request->get('order_id'),
                'type' => $request->get('type_payment'),
                'status' => $request->get('status'),
            ]);
            $payment->save();
            return \App\Http\Resources\Payment::make($payment);
        } catch (\Exception $e)
        {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $payment  = Payment::find($id);
            if($payment){
                $payment->type = $request->get('type', $payment->type);
                $payment->status = $request->get('status', $payment->status);
                $payment->update();
                if($payment->status == Payment::STATUS_PAID){
                    $order = Order::find($payment->order_id);
                    $order->status = Order::STATUS_ORDER_CANCEL;
                }
                return response()->json([
                    'status' => true,
                    'message' => 'update payment successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'update failed'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Payment::destroy($id);
            return response()->json([
            'status' => true,
            'message' => 'delete payment successfully'
        ]);

        } catch (\Exception $e)
        {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
