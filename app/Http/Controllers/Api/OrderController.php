<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $orders = Order::all();
       return \App\Http\Resources\Order::collection($orders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_CUSTOMER)
        {
            $now = date('Y/m/d');
            $dateShipped = date('Y/m/d', strtotime("$now +3 day"));
            $order = new Order([
                'product_id' => $request->get('product_id'),
                'user_id' => $user->id,
                'amount' => $request->get('amount'),
                'total_price' => $request->get('total_price'),
                'status' => Order::STATUS_ORDER_PENDING,
                'shipped_date' => $dateShipped,
            ]);
            $order->save();
            $payment = new Payment([
                'order_id' => $order->id,
                'type' => Payment::TYPE_OFF,
                'status' => Payment::STATUS_UNPAID,
            ]);
            $payment->save();
            return response()->json([
                'status' => true,
                'payment' => \App\Http\Resources\Payment::make($payment)
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'create order failed'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        if($order) return \App\Http\Resources\Order::make($order);
        return response()->json([
            'status' => true,
            'message' => 'No data!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $order = Order::find($id);
            $order->amount = $request->get('amount', $order->amount);
            $order->status = $request->get('status', $order->status);
            $order->shipped_date = $request->get('shipped_date', $order->shipped_date);
            $order->update();
            return response()->json([
                'status' => true,
                'message' => 'update order successfully'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $order = Order::find($id);
            if($order) {
                $payment = Payment::where('order_id', '=', $order->id)->get();
                if($payment) {
                    Payment::destroy($payment->id);
                }
                Order::destroy($id);
                return response()->json([
                    'status' => true,
                    'message' => 'delete order successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'delete order failed'
            ]);
        } catch (\Exception $e){
            return response()->json([
                'status' => false,
                'message' => 'delete order failed'
            ]);
        }
    }
    public function getOrderByUser()
    {
        try {
            $user = JWTAuth::user();
            $user_id = $user->id;
            $orders = Order::where('user_id', '=', $user_id)->get();
            return \App\Http\Resources\Order::collection($orders);
        }
        catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
