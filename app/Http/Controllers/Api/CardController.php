<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cards = Card::all();
        return \App\Http\Resources\Card::collection($cards);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_CUSTOMER)
        {
            $card = new Card([
                'product_id' => $request->get('product_id'),
                'user_id' => $user->id,
                'amount' => $request->get('amount'),
                'total_price' => $request->get('total_price')
            ]);
            $card->save();
            return response()->json([
                'status' => true,
                'message' => 'add to card successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'add to card failed'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $card = Card::find($id);
        if($card)  return \App\Http\Resources\Card::make($card);
        return response()->json([
            'status' => true,
            'message' => 'No data!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $card = Card::find($id);
            $card->amount = $request->get('amount', $card->amount);
            $card->total_price = $request->get('total_price', $card->total_price);
            $card->update();
            return response()->json([
                'status' => true,
                'message' => 'update card successfully'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status' => false,
                'message' => 'update card failed'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $card = Card::destroy($id);
        if($card) {
            return response()->json([
                'status' => true,
                'message' => 'delete card successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'delete card failed'
        ]);

    }
    public function getCardByUser()
    {
        try {
            $user = JWTAuth::user();
            $user_id = $user->id;
            $cards = Card::where('user_id', '=', $user_id)->get();
            return \App\Http\Resources\Card::collection($cards);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
    public function deleteCardUser()
    {
        try {
            $user = JWTAuth::user();
            $user_id = $user->id;
            $cards = Card::where('user_id', '=', $user_id)->get();
            foreach ($cards as $card){
                Card::destroy($card->id);
            }
            return response()->json([
                'status' => true,
                'message' => 'delete successfully'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

}
