<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        return \App\Http\Resources\Category::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_ADMIN)
        {
            $newCategory = new Category([
                'user_create' => $user->id,
                'name' => $request->get('name'),
                'description' => $request->get('description'),
            ]);
            $newCategory->save();
            return response()->json([
                'status' => true,
                'message' => 'create category successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'create category failed'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        if($category)
        {
            return \App\Http\Resources\Category::make($category);
        }
        return response()->json([
            'status' => true,
            'message' => 'No data!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = JWTAuth::user();
            if($user->type === User::TYPE_ADMIN)
            {
                $category = Category::find($id);
                $category->user_create = $user->id;
                $category->name = $request->get('name', $category->name);
                $category->description = $request->get('description', $category->description);
                $category->update();
                return response()->json([
                    'status' => true,
                    'message' => 'update category successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'update category failed'
            ]);
        } catch (\Exception $exception)
        {
            return response()->json([
                'status' => false,
                'message' => 'update category failed'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::destroy($id);
        if($category)
        {
            return response()->json([
                'status' => true,
                'message' => 'delete category successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'delete category failed'
        ]);
    }
}
