<?php

namespace App\Http\Controllers\Api;

use App\Custom\Utils;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Order;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::where('id', '>', 0);
        if($request->get('sort')){
            if($request->get('sort') == 'low_to_high')
            {
                $products = $products->orderBy('price', 'ASC');
            }
            if($request->get('sort') == 'high_to_low')
            {
                $products = $products->orderBy('price', 'DESC');
            }
        }
        if($request->get('search_name'))
        {
            $name = $request->get('search_name');
            $products = $products->where('name', 'like', '%'. $name .'%');
        }
        if($request->get('price_from')){
            $products = $products->where('price', '>=', $request->get('price_from'));
        }
        if($request->get('price_to')){
            $products = $products->where('price', '<=', $request->get('price_to'));
        }
        $products = $products->get();
        return \App\Http\Resources\Product::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_ADMIN)
        {
            $product = new Product([
                'category_id' => $request->get('category_id'),
                'user_create' => $user->id,
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'quantity' => $request->get('quantity'),
                'price' => $request->get('price'),
                'discount_price' => $request->get('discount_price'),
            ]);
            $product->save();
            if($request->file('product_file_main')){
                $file = $request->file('product_file_main');
                if(Utils::checkExtensionFile($file) == false) return response()->json(['message'=>'file invalid']);
                Utils::createNewRecordFileProduct($file, $product->id, File::TYPE_FILE_MAIN);
            }
            if($request->hasFile('product_file_secondary'))
            {
                $files = $request->file('product_file_secondary');
                foreach ($files as $item)
                {
                    if(Utils::checkExtensionFile($item) == false) return response()->json(['message'=>'file invalid']);
                    Utils::createNewRecordFileProduct($item, $product->id, File::TYPE_FILE_SECONDARY);
                }
            }
            return response()->json([
               'status' => true,
               'message' => 'create product successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'create product failed'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if($product)
        {
            return \App\Http\Resources\Product::make($product);
        }
        return response()->json([
            'status' => true,
            'message' => 'No data!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = JWTAuth::user();
            if($user->type === User::TYPE_ADMIN) {
                $product = Product::find($id);
                $product->category_id = $request->get('category_id', $product->category_id);
                $product->user_create = $user->id;
                $product->name = $request->get('name', $product->name);
                $product->description = $request->get('description', $product->description);
                $product->quantity = $request->get('quantity', $product->quantity);
                $product->price = $request->get('price', $product->price);
                $product->discount_price = $request->get('discount_price',$product->discount_price);
                $fileProductMain = File::where('obj_model', '=', File::OBJ_MODEL_PRODUCT)
                    ->where('product_id', '=', $product->id)->where('type', '=', File::TYPE_FILE_MAIN)->first();
                $fileProductSeconds = File::where('obj_model', '=', File::OBJ_MODEL_PRODUCT)
                    ->where('product_id', '=', $product->id)->where('type', '=', File::TYPE_FILE_SECONDARY)->get();
                if($request->file('product_file_main')){
                    $fileMain = $request->file('product_file_main');
                    if(Utils::checkExtensionFile($fileMain) == false) return response()->json(['message'=>'file invalid']);
                    Utils::updateRecordFile($fileMain, $fileProductMain->id);
                }
                if($request->hasFile('product_file_secondary'))
                {
                    $fileSeconds = $request->file('product_file_secondary');
                    foreach ($fileProductSeconds as $fileProductSecond)
                    {
                        Utils::deleteRecordFile($fileProductSecond->id);
                    }
                    foreach ($fileSeconds as $item)
                    {
                        if(Utils::checkExtensionFile($item) == false) return response()->json(['message'=>'file invalid']);
                        Utils::createNewRecordFileProduct($item, $product->id, File::TYPE_FILE_SECONDARY);
                    }
                }
                $product->update();
                return response()->json([
                    'status' => true,
                    'message' => 'update product successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'update product failed'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'update product failed'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_ADMIN) {
            $product = Product::find($id);
            $productOrder = Order::where('product_id', '=', $id)->count();
            if($productOrder > 0) {
                return response()->json([
                    'status' => 'false',
                    'message' => 'delete product failed'
                ]);
            }
            $productReviews = Review::where('obj_model', '=', Review::TYPE_PRODUCT)
                            ->where('product_id', '=', $id)->count();
            if($productReviews > 0)
            {
                return response()->json([
                    'status' => 'false',
                    'message' => 'delete product failed'
                ]);
            }
            $fileProducts = File::where('obj_model', '=', File::OBJ_MODEL_PRODUCT)
                ->where('product_id', '=', $product->id)->get();
            foreach ($fileProducts as $fileProduct)
            {
                Utils::deleteRecordFile($fileProduct->id);
            }
            Product::destroy($product->id);
            return response()->json([
                'status' => true,
                'message' => 'delete product successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'delete product failed'
        ]);
    }
    public function getProductByCategory($id, Request $request)
    {
        try {
            $products = Product::where('category_id', '=', $id);
            if($request->get('sort_name') == 'low_to_high') {
                $products = $products->orderBy('name');
            }
            if($request->get('sort_name') == 'high_to_low') {
                $products = $products->orderBy('name', 'desc');
            }
            if($request->get('sort_price') == 'low_to_high') {
                $products = $products->orderBy('price');
            }
            if($request->get('sort_price') == 'high_to_low') {
                $products = $products->orderBy('price', 'desc');
            }
            if($products->count() > 0){
                return response()->json([
                    'status' => true,
                    'data' => \App\Http\Resources\Product::collection($products->get())
                ]);
            }
            return response()->json([
                'status' => true,
                'message' => 'no data'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'delete product failed'
            ]);
        }

    }
    public function getProductToPrice(Request $request)
    {
        $products = Product::where('id', '>', 0);
        if($request->get('price_from')){
           $products = $products->where('price', '>=', $request->get('price_from'));
        }
        if($request->get('price_to')){
            $products = $products->where('price', '<=', $request->get('price_to'));
        }
        $products->get();
        return \App\Http\Resources\Product::collection($products);
    }
}
