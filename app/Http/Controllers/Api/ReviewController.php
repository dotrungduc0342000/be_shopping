<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reviews = Review::all();
        return \App\Http\Resources\Review::collection($reviews);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();
        if($user->type === User::TYPE_CUSTOMER)
        {
            $productId = $request->get('product_id');
            $product = Product::find($productId);
            if($product) {
                $review = new Review([
                    'product_id' => $productId,
                    'user_id' => $user->id,
                    'obj_model' => 'product',
                    'content' => $request->get('content'),
                    'rate_number' => $request->get('rate_number'),
                    'status' => Review::STATUS_PUBLISH,
                ]);
                $review->save();
            }
            return response()->json([
                'status' => true,
                'message' => 'create review successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'create review failed'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);
        if($review) return  \App\Http\Resources\Review::make($review);
        return response()->json([
            'status' => true,
            'message' => 'No data!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = JWTAuth::user();
            if($user->type === User::TYPE_ADMIN)
            {
                $review = Review::find($id);
                $review->product_id = $request->get('product_id', $review->product_id);
//                $review->user_id = $request->get('user_id');
                $review->obj_model = 'product';
                $review->content = $request->get('content',  $review->content);
                $review->rate_number = $request->get('rate_number',  $review->rate_number);
                $review->status = $request->get('status', $review->status);
                $review->update();
                return response()->json([
                    'status' => true,
                    'message' => 'update review successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'update review failed'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'update review failed'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::destroy($id);
        if($review) {
            return response()->json([
                'status' => true,
                'message' => 'delete review successfully'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'delete review failed'
        ]);
    }
    public function getReviewByProduct($id)
    {
        try {
            $reviews = Review::where('obj_model', '=', 'product')->where('product_id', '=', $id)->get();
            return \App\Http\Resources\Review::collection($reviews);
        }
        catch (\Exception $e) {
            return response()->json([
                'status' => false,
               'message' => $e->getMessage()
            ]);
        }

    }
}
