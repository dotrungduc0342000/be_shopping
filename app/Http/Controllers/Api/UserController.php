<?php

namespace App\Http\Controllers\Api;

use App\Custom\Utils;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::all();
        return \App\Http\Resources\User::collection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|string|min:6',
                'type' => 'required',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            $user = JWTAuth::user();
            if($user->type === User::TYPE_ADMIN) {
                $user = new User([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'type' => $request->get('type'),
                    'phone' => $request->get('phone'),
                    'address' => $request->get('address'),
                ]);
                $user->save();
                return response()->json([
                    'message' => 'User create successfully',
                    'user' => $user
                ], 200);
            }
            return \response()->json([
                'status' => false,
                'message' => 'User create failed'
            ], 500);
        } catch (\Exception $e){
            return \response()->json([
                'status' => false,
                'message' => 'User create failed'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->first_name = $request->get('first_name', $user->first_name);
            $user->last_name = $request->get('last_name', $user->last_name);
            $user->phone = $request->get('phone', $user->phone);
            $user->address = $request->get('address', $user->address);
            $file = File::where('obj_model', '=', File::OBJ_MODEL_USER)
                ->where('product_id', '=', $user->id)->first();
            if($request->file('avatar')) {
                $avatar = $request->file('avatar');
                if(Utils::checkExtensionFile($avatar) == false) return response()->json(['message'=>'file invalid']);
                if($file) {
                    Utils::updateRecordFile($avatar, $file->id);
                } else {
                    Utils::createNewRecordFileUser($avatar, $user->id);
                }
            }
            $user->update();
            return response()->json([
                'message' => 'User update successfully',
                'user' => $user
            ], 200);
        } catch (\Exception $e){
            return \response()->json([
                'status' => false,
                'message' => 'User update failed'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = JWTAuth::user();
            if($user->type === User::TYPE_ADMIN) {
                $file = File::where('obj_model', '=', File::OBJ_MODEL_USER)
                    ->where('product_id', '=', $user->id)->first();
                if($file) {
                    File::destroy($file->id);
                }
                User::destroy($id);
                return response()->json([
                    'message' => 'Delete user successfully',
                    'user' => $user
                ], 200);
            }
            return \response()->json([
                'status' => false,
                'message' => 'Delete user failed'
            ], 500);
        } catch (\Exception $e){
            return \response()->json([
                'status' => false,
                'message' => 'Delete user failed'
            ], 500);
        }
    }
    public function getUserInfo(Request $request){
        try {
            $user = JWTAuth::authenticate($request->token);
            return \App\Http\Resources\User::make($user);
        } catch (\Exception $e){
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
