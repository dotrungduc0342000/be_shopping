<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        $user = JWTAuth::user();
        return response()->json([
            'status' => true,
            'token' => $token,
            'data' => $user,
        ]);
    }
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'status' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }
    public function register(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|confirmed|string|min:6',
            ]);
            if($validator->fails()){
                return \response()->json([
                    'status' => false,
                    'message' => 'registered failed'
                ]);
            }
                $user = new User([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'type' => User::TYPE_CUSTOMER,
                    'phone' => $request->get('phone'),
                    'address' => $request->get('address'),
                ]);
                $user->save();
                return response()->json([
                    'status' => true,
                    'message' => 'User successfully registered',
                    'user' => $user
                ], 201);

        } catch (\Exception $e){
            return \response()->json([
               'status' => false,
               'message' => 'registered failed'
            ], 500);
        }
    }

}
