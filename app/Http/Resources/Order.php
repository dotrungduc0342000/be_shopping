<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => Product::make($this->getProduct),
            'user' => User::make($this->getUser),
            'amount' => $this->amount,
            'total_price' => $this->total_price,
            'status' => $this->status,
            'shipped_date' => $this->shipped_date,
        ];
    }
}
