<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Review extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => Product::make($this->getProduct),
            'user' => User::make($this->getUser),
            'obj_model' => $this->obj_model,
            'content' => $this->content,
            'rate_number' => $this->rate_number,
            'status' => $this->status,
        ];
    }
}
