<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    use HasFactory;
    const STATUS_READ = 'read';
    const STATUS_UNREAD = 'unread';
    public $table = "notify";

    protected $fillable = [
        'obj_id',
        'obj_model',
        'content',
        'content'
    ];

}
