<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $table = "product";
    protected $fillable = [
        'category_id',
        'user_create',
        'name',
        'description',
        'quantity',
        'price',
        'discount_price',
    ];
    public function getCategory()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function getUserCreate()
    {
        return $this->belongsTo(User::class, 'user_create', 'id');
    }
    public function getFile($id)
    {
        $files = File::where('obj_model', '=', 'product')->where('product_id', '=', $id)->get();
        $data = [];
        foreach ($files as $file)
        {
            $data[] = [
                'type' => $file->type,
                'link' => url($file->file_path)
            ];
        }
        return $data;
    }

}
