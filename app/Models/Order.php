<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    const STATUS_ORDER_PENDING = 'pending';
    const STATUS_ORDER_CANCEL = 'cancel';
    public $table = "order";
    protected $fillable = [
        'product_id',
        'user_id',
        'amount',
        'total_price',
        'status',
        'shipped_date',
    ];
    public function getProduct()
    {
       return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function getUser()
    {
       return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
