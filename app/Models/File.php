<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    const STATUS_PUBLISH = 'publish';
    const STATUS_DRAFT = 'draft';
    const OBJ_MODEL_USER = 'user';
    const OBJ_MODEL_PRODUCT = 'product';
    const TYPE_FILE_MAIN = 'main';
    const TYPE_FILE_SECONDARY = 'secondary';
    public $table = "file";
    protected $fillable = [
        'obj_model',
        'user_id',
        'product_id',
        'name',
        'mine',
        'file_path',
        'type',
        'status',
    ];
    public function getFileUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getFileProduct(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

}
