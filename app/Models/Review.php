<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    const STATUS_PUBLISH = 'publish';
    const STATUS_SPAM = 'spam';
    const TYPE_PRODUCT = 'product';
    public $table = "review";
    protected $fillable = [
        'product_id',
        'user_id',
        'obj_model',
        'content',
        'rate_number',
        'status',
    ];
    public function getProduct()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
