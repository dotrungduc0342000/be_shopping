<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public $table = "category";
    protected $fillable = [
        'user_create',
        'name',
        'description',
    ];
    public function getUserCreate()
    {
        return $this->belongsTo(User::class, 'user_create', 'id');
    }

}
