<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    const STATUS_PAID = 'paid';
    const STATUS_UNPAID = 'unpaid';
    const TYPE_OFF = 'off';
    public $table = "payment";
    protected $fillable = [
        'order_id',
        'type',
        'status',
    ];
    public function getOrder()
    {
       return $this->belongsTo(Order::class, 'order_id', 'id');
    }
}
