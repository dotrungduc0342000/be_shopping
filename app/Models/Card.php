<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;
    public $table = "card";

    protected $fillable = [
        'product_id',
        'user_id',
        'amount',
        'total_price'
    ];

    public function getProduct()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function getUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
