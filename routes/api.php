<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login'])->name('users.login');
Route::post('/register', [\App\Http\Controllers\Api\AuthController::class, 'register'])->name('users.register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout'])->name('users.logout');
    Route::get('/users', [\App\Http\Controllers\Api\UserController::class, 'index'])->name('users.index');
    Route::post('/user/store', [\App\Http\Controllers\Api\UserController::class, 'store'])->name('users.store');
    Route::post('/user/update/{id}', [\App\Http\Controllers\Api\UserController::class, 'update'])->name('users.update');
    Route::post('/user/delete/{id}', [\App\Http\Controllers\Api\UserController::class, 'destroy'])->name('users.destroy');
    Route::get('/getUserInfo', [\App\Http\Controllers\Api\UserController::class, 'getUserInfo'])->name('users.getUserInfo');
    //product
    Route::post('/product/store', [\App\Http\Controllers\Api\ProductController::class, 'store'])->name('product.store');
    Route::post('/product/update/{id}', [\App\Http\Controllers\Api\ProductController::class, 'update'])->name('product.update');
    Route::post('/product/delete/{id}', [\App\Http\Controllers\Api\ProductController::class, 'destroy'])->name('product.destroy');
    //category
    Route::post('/category/store', [\App\Http\Controllers\Api\CategoryController::class, 'store'])->name('category.store');
    Route::post('/category/update/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'update'])->name('category.update');
    Route::post('/category/delete/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'destroy'])->name('category.destroy');
    //review
    Route::post('/review/store', [\App\Http\Controllers\Api\ReviewController::class, 'store'])->name('review.store');
    Route::post('/review/update/{id}', [\App\Http\Controllers\Api\ReviewController::class, 'update'])->name('review.update');
    Route::post('/review/delete/{id}', [\App\Http\Controllers\Api\ReviewController::class, 'destroy'])->name('review.update');
    //card
    Route::post('/card/store', [\App\Http\Controllers\Api\CardController::class, 'store'])->name('cards.store');
    Route::post('/card/delete/{id}', [\App\Http\Controllers\Api\CardController::class, 'destroy'])->name('cards.delete');
    Route::post('/card/update/{id}', [\App\Http\Controllers\Api\CardController::class, 'update'])->name('cards.update');
    Route::get('/card/user', [\App\Http\Controllers\Api\CardController::class, 'getCardByUser'])->name('cards.user');
    Route::post('/card/delete/all/user', [\App\Http\Controllers\Api\CardController::class, 'deleteCardUser'])->name('cards.deleteCardUser');
    //order
    Route::post('/order/store', [\App\Http\Controllers\Api\OrderController::class, 'store'])->name('orders.store');
    Route::post('/order/update/{id}', [\App\Http\Controllers\Api\OrderController::class, 'update'])->name('orders.update');
    Route::post('/order/delete/{id}', [\App\Http\Controllers\Api\OrderController::class, 'destroy'])->name('orders.delete');
    Route::get('/order/user', [\App\Http\Controllers\Api\OrderController::class, 'getOrderByUser'])->name('orders.getOrderByUser');
    //payment
    Route::post('/payment/store', [\App\Http\Controllers\Api\PaymentController::class, 'store'])->name('payments.store');
    Route::post('/payment/update/{id}', [\App\Http\Controllers\Api\PaymentController::class, 'update'])->name('payments.update');
    Route::post('/payment/delete/{id}', [\App\Http\Controllers\Api\PaymentController::class, 'destroy'])->name('payments.destroy');
});

Route::get('/categories', [\App\Http\Controllers\Api\CategoryController::class, 'index'])->name('categories.index');
Route::get('/category/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'show'])->name('category.show');

Route::get('/products', [\App\Http\Controllers\Api\ProductController::class, 'index'])->name('products.index');
Route::get('/product/{id}', [\App\Http\Controllers\Api\ProductController::class, 'show'])->name('product.show');
Route::get('/product/category/{id}', [\App\Http\Controllers\Api\ProductController::class, 'getProductByCategory'])->name('product.getProductByCategory');
Route::get('/product/price', [\App\Http\Controllers\Api\ProductController::class, 'getProductToPrice'])->name('product.getProductToPrice');

Route::get('/cards', [\App\Http\Controllers\Api\CardController::class, 'index'])->name('cards.index');
Route::get('/card/show/{id}', [\App\Http\Controllers\Api\CardController::class, 'show'])->name('cards.show');

Route::get('/orders', [\App\Http\Controllers\Api\OrderController::class, 'index'])->name('orders.index');
Route::get('/order/show/{id}', [\App\Http\Controllers\Api\OrderController::class, 'show'])->name('orders.show');

Route::get('/payments', [\App\Http\Controllers\Api\PaymentController::class, 'index'])->name('payments.index');
Route::get('/payment/update/{id}', [\App\Http\Controllers\Api\PaymentController::class, 'update'])->name('payments.update');
Route::get('/payment/delete/{id}', [\App\Http\Controllers\Api\PaymentController::class, 'destroy'])->name('payments.delete');

Route::get('/reviews', [\App\Http\Controllers\Api\ReviewController::class, 'index'])->name('reviews.index');
Route::get('/review/{id}', [\App\Http\Controllers\Api\ReviewController::class, 'show'])->name('reviews.show');
Route::get('/review/product/{id}', [\App\Http\Controllers\Api\ReviewController::class, 'getReviewByProduct'])->name('reviews.getReviewByProduct');

Route::get('/notifies', [\App\Http\Controllers\Api\NotifyController::class, 'index'])->name('notifies.index');

