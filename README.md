Setup Environment:
    step 1 : clone code
    step 2 : php artisan key:generate
    step 3: php artisan migrate
    step 4: php artisan db:seed
    step 5: php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    step 6: php artisan jwt:secret


Link figma: https://www.figma.com/file/EkWD21LisqIFVHMHgzTdEU/BTL?node-id=0%3A1&fbclid=IwAR2_Ya8k6abH8f_8y1BZrTqoEmcU40C2atGRAER1aYEPYVSIhiiBHjAOp7g
